# Leslie Kerby
# CS 1181
# Chapter 18: Fractions (more classes/objects)

class Fraction:
    '''docstring here: Fraction contains a fraction'''
    def __init__(self, top, bottom):
        self.num = top
        self.den = bottom
    def getNum(self):
        return self.num
    def getDen(self):
        return self.den
    def __str__(self):   # overloaded __str__
        return (str(self.num) + '/' + str(self.den))
    def simplify(self):   # mutator method (ie changes the state of object)
        common = gcd(self.num, self.den)
        self.num = self.num // common
        self.den = self.den // common
    def sameFraction(self, f2):
        #return (self.num == f2.getNum()) & (self.den == f2.getDen())
        # not robust ^^, doesn't work for fractions that aren't simplified
        return (self.num * f2.getDen() == self.den * f2.getNum())
    def add(self, f2):
        newnum = self.num * f2.getDen() + self.den * f2.getNum()
        newden = self.den * f2.getDen()
        #newFraction = Fraction(newnum, newden)
        #newFraction.simplify()
        #return newFraction
        # OR
        common = gcd(newnum, newden)
        return Fraction(newnum//common, newden//common)
    def __add__(self, f2):  # overload '+' operator
        newnum = self.num * f2.getDen() + self.den * f2.getNum()
        newden = self.den * f2.getDen()
        common = gcd(newnum, newden)
        return Fraction(newnum//common, newden//common)
    def __sub__(self, f2):  # overload '-' operator
        newnum = self.num * f2.getDen() - self.den * f2.getNum()
        newden = self.den * f2.getDen()
        common = gcd(newnum, newden)
        return Fraction(newnum//common, newden//common)
    def copy(self):  # creates deep copy
        return Fraction(self.num, self.den)

def gcd(t, b):    # Called a 'helper' function (for the Fraction class)
    '''Finds the greatest common divisor'''
    while t % b != 0:       # First | Second |    # First | Second | Third
        oldt = t            #   12  |  16             11  |  7    |  4
        oldb = b            #   16  |  12              7  |  4    |  3

        t = oldb            #   16  |  12              7  |  4    |  3
        b = oldt % oldb     #   12  |  4               4  |  3    |  1

    return b

frac = Fraction(6,12)
print(frac)
#frac.simplify()
print(frac)
print(frac.sameFraction(Fraction(6,12)))

print('*************')
f2 = Fraction(3, 5)
sum = frac.add(f2)
print(sum)
print(frac + f2)
print(frac - f2)

print('*************')
f3 = frac   # this is utilizing the __copy__ method, default creates a shallow copy
print(f3)
print(f3 is frac)
f3 = frac.copy()    # our copy method creates a deep copy
print(f3 is frac)

# Try accessing Fraction state outside of the class
print(frac.num)
# For many languages (C++, Java) this would give you an error
# Best practice says do NOT do this -- don't access object state outside of
#     object, in general -- use getter and setter methods instead:
print(frac.getNum())
