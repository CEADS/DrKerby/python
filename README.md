# Python Examples

**Workshop Notebooks:** <br>
[Python Introduction and NumPy](https://colab.research.google.com/drive/1DeLhSyUnEGZb16b8aczvrWWOeM3CXhS6) <br>
[Pandas, Functions, and Histograms](https://colab.research.google.com/drive/16Lz67ooPZdjwUnjQTDntbg4vzfOoxLRf) <br>
[Sampling](https://colab.research.google.com/drive/1AVGUTWlGHu2XN7xWCr1onfZvSaq9zE5t) <br>
[Pandas GroupBy](https://colab.research.google.com/drive/1MqF-O-oXY2xiFaXWwc8jUjNf-qgSqSFw) <br>
[Statistics and Regression](https://colab.research.google.com/drive/10ofi8qzolJIOWLvJ5pjdVXgZRVbM4Ewr) <br>
[Linear Regression](https://colab.research.google.com/drive/1FkIlc2aHDAXwL0X71VMIYrHWqjOhL6V7) <br>
[Predicting with Decision Trees](https://colab.research.google.com/drive/1Dc0RKjHN0cbcW6mtCEJ9ncFiOtFGgUlS#scrollTo=XPeT2dOdOJsU) <br>
[Credit Card Defaults, Cleaning and Feature Engineering](https://colab.research.google.com/drive/1uO12S-myWW_o0xPNV9KmTffHJ35HPoOp) <br>
[Credit Card Defaults with Logistic Regression, Random Forests, and Naive Bayes](https://colab.research.google.com/drive/1JjjUd7yKwSWm2jPrqnYQvSsxliWLx7ze) <br>
[Clustering with kMeans](https://colab.research.google.com/drive/1qi8OjUFFj-ZY0Ysk3dKPY47d4bjwYtoM) <br>

Link to Dr. Josh Peterson-Droogh's [website](https://www.gitlab.com/nukespud/data-science) and his notebooks

[Introduction to Data Science](https://docs.google.com/presentation/d/130KE_aB-2IcaVUgWJykZSFqLONGS4LL9tOEEM6NRgYs/edit?usp=sharing) slides <br>
[Introduction to Machine Learning](https://docs.google.com/presentation/d/147kSgdaI07_DkhEFiwJhno7gj_uKG8SJouPIYlEG4NI/edit?usp=sharing) slides

Links to MNIST Digits and AutoML notebooks:<br>
[MNIST with traditional ML](https://colab.research.google.com/drive/18sZvSPRGZ6VFD2R_SpndaA0W5LI-BEtC)<br>
[MNIST with TPOT](https://colab.research.google.com/drive/1RR4zFSFpjSfCuOb8BXobvP8KjsNfYxP7)<br>
[MNIST with Autokeras](https://colab.research.google.com/drive/1KaRg0r4RnLcT4mbyrZkgR3nxPos9KzBl)<br>


