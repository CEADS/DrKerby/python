# Leslie Kerby
# CS 1181
# Chapter 17: Objects

class Point:
    '''Point class for representing and manipulating (x,y) coordinates'''
    def __init__(self, initX=0, initY=0):  # constructor
        '''Create new point at (initX, initY); default of (0,0)'''
        self.x = initX
        self.y = initY
    def __str__(self):   # Overloading the __str__ function
        return f'({self.getX()}, {self.getY()})'
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def setX(self, newX):
        self.x = newX
    def setY(self, newY):
        self.y = newY
    def distanceFromOrigin(self):
        return (self.x**2 + self.y**2)**0.5
    def distanceFromPoint(self, point2):   # OOP way
        deltaX = self.getX() - point2.getX()
        deltaY = self.getY() - point2.getY()
        return (deltaX**2 + deltaY**2)**0.5
    def midPoint(self, point2):
        deltaX = point2.getX() - self.getX()
        deltaY = point2.getY() - self.getY()
        newX = self.getX() + deltaX/2
        newY = self.getY() + deltaY/2
        return Point(newX, newY)

def distance(point1, point2):  # procedural/functional programming way
    deltaX = point1.getX() - point2.getX()
    deltaY = point1.getY() - point2.getY()
    return (deltaX**2 + deltaY**2)**0.5

p = Point()
print(p)
print(p.getX(), p.getY())

q = Point(3,4)
print(q.getX(), q.getY())
print(q.distanceFromOrigin())

s = Point(7,6)
print(s.getX(), s.getY())
print(s.distanceFromOrigin())
print(s.distanceFromPoint(q))
print(distance(s,q))

s.setX(-7)
print(s.getX(), s.getY())
print(s.distanceFromOrigin())
print(s.distanceFromPoint(q))
print(distance(s,q))

print('******************')
m = s.midPoint(q)
print(m.getX(), m.getY())
print(m)
