# Leslie Kerby
# CS 1181
# Chapter 19: Pillars of OOP

class Point:
    '''Point class for representing and manipulating (x,y) coordinates'''
    def __init__(self, initX=0, initY=0):  # constructor
        '''Create new point at (initX, initY); default of (0,0)'''
        self.x = initX
        self.y = initY
    def __str__(self):   # Overloading the __str__ function
        return f'({self.getX()}, {self.getY()})'
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def setX(self, newX):
        self.x = newX
    def setY(self, newY):
        self.y = newY
    def distanceFromOrigin(self):
        return (self.x**2 + self.y**2)**0.5
    def distanceFromPoint(self, point2):   # OOP way
        deltaX = self.getX() - point2.getX()
        deltaY = self.getY() - point2.getY()
        return (deltaX**2 + deltaY**2)**0.5
    def midPoint(self, point2):
        deltaX = point2.getX() - self.getX()
        deltaY = point2.getY() - self.getY()
        newX = self.getX() + deltaX/2
        newY = self.getY() + deltaY/2
        return Point(newX, newY)
'''
# Derived LabeledPoint (using inheritance)
class LabeledPoint(Point):
    def __init__(self, initX, initY, label):  # overrides Point __init__
        super().__init__(initX, initY)   # can access superclass methods with super()
        self.label = label
    def __str__(self):   # this overrides the Point __str__
        return super().__str__() + ' "' + self.label + '"'
'''
# Composed LabeledPoint (using composition)
class LabeledPoint:
    def __init__(self, initX, initY, label):
        self.point = Point(initX, initY)
        self.label = label
    def __str__(self):
        return str(self.point) + ' "' + self.label + '"'
    def distanceFromOrigin(self):
        return self.point.distanceFromOrigin()

p = Point(3, 4)
print(p)
lp = LabeledPoint(5, 6, 'Over There')
print(lp)
print(lp.distanceFromOrigin())
